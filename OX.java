import java.util.*;

public class OX {

	static String player = "X";
	static int round;
	
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int row,col;
		String again="Y";
		char[][] board = new char[3][3];
		while(again.equals("Y")) {
			newBoard(board);
			showWelcome();
			while(round <10) {
				showBoard(board);
				showTurn();
				inputNumber(board);
				checkWin(board);
				round++;
			}		
			showBoard(board);
			showAgain();
			again = kb.next();
		}
		showEndgame();
	}
	private static void showEndgame() {
		System.out.println("\nThank you for playing \"XO Game\"");
		
	}
	public static void showWelcome() {
		System.out.println("=====  Welcome to XO GAME  ===== ");
	}
	public static void showBoard(char[][] board) {
		System.out.println("+---+---+---+");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("| ");
				System.out.print(board[i][j]);
				System.out.print(" ");
			}
			System.out.print("| ");
			System.out.println();
			System.out.println("+---+---+---+");
		}
	}
	public static void showScore(int owin,int xwin) {
		System.out.println("\n===== Score =====");
		System.out.println("O's score = "+owin);
		System.out.println("X's score = "+xwin);
		System.out.println("\nThank you for playing \"XO Game\"");
	}
	public static void showAgain() {
		System.out.println("Do you want to play again? (N/Y)");
		round=1;
	}
	public static void showTurn() {
		System.out.println(player+"'s Turn");
	}
	public static void inputNumber(char[][] board) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Please input Row and Column : ");
		int row = kb.nextInt()-1;
		int col = kb.nextInt()-1;
		System.out.println();
		checkInput(board,row,col);
	}
	public static void changePlayer(char[][] board,int row,int col) {
		if(player.equals("X")) {
			board[row][col] = 'X';
			player = "O";
		}else if(player.equals("O")) {
			board[row][col] = 'O';
			player = "X";
		}
	}
	public static void checkInput(char[][] board,int row,int col) {
		if (row > 2 || row < 0) {
			System.out.println("====Number of Row error!! Please input only 1, 2 or 3====\n");
		} else if (col > 2 || col < 0) {
			System.out.println("====Number of Column error!! Please input only 1, 2 or 3====\n");
		} else if (board[row][col] == 'O' || board[row][col] == 'X') {
			System.out.println("====This place is already taken!! Please try again ====\n");
		} else {
			changePlayer(board,row,col);
		}
	}
	public static void checkWin(char[][] board) {
		checkXwin(board);
		checkOwin(board);
		checkDraw(board);
	}

	public static void checkXwin(char[][] board){
		checkXwinHorizon(board);
		checkXwinvertical(board);
		checkXwindiagonal(board);

		
	}
	public static void checkXwinHorizon(char[][] board) {
		if(board[0][0]=='X' && board[0][1]=='X' && board[0][2]=='X')
			addXWinner();
		else if (board[1][0]=='X' && board[1][1]=='X' && board[1][2]=='X')
			addXWinner();
		else if (board[2][0]=='X' && board[2][1]=='X' && board[2][2]=='X')
			addXWinner();
	}
	public static void checkXwinvertical(char[][] board) {
		if(board[0][0]=='X' && board[1][0]=='X' && board[2][0]=='X')
			addXWinner();
		else if(board[0][1]=='X' && board[1][1]=='X' && board[2][1]=='X')
			addXWinner();
		else if(board[0][2]=='X' && board[1][2]=='X' && board[2][2]=='X')
			addXWinner();
	}
	public static void checkXwindiagonal(char[][] board) {
		if(board[0][0]=='X' && board[1][1]=='X' && board[2][2]=='X')
			addXWinner();
		else if(board[0][2]=='X' && board[1][1]=='X' && board[2][0]=='X')
			addXWinner();
	}
	public static void addXWinner() {
		System.out.println("\n===== X is the winner!! =====\n");
		round=10;	
	}
	public static void checkOwin(char[][] board) {			
		checkOwinHorizon(board);
		checkOwinvertical(board);
		checkOwindiagonal(board);
	}
	public static void checkOwinHorizon(char[][] board) {
		if(board[0][0]=='O' && board[0][1]=='O' && board[0][2]=='O')
			addOWinner();
		else if (board[1][0]=='O' && board[1][1]=='O' && board[1][2]=='O')
			addOWinner();
		else if (board[2][0]=='O' && board[2][1]=='O' && board[2][2]=='O')
			addOWinner();
	}
	public static void checkOwinvertical(char[][] board) {
		if(board[0][0]=='O' && board[1][0]=='O' && board[2][0]=='O')
			addOWinner();
		else if(board[0][1]=='O' && board[1][1]=='O' && board[2][1]=='O')
			addOWinner();
		else if(board[0][2]=='O' && board[1][2]=='O' && board[2][2]=='O')
			addOWinner();
	}
	public static void checkOwindiagonal(char[][] board) {
		if(board[0][0]=='O' && board[1][1]=='O' && board[2][2]=='O')
			addOWinner();
		else if(board[0][2]=='O' && board[1][1]=='O' && board[2][0]=='O')
			addOWinner();
	}
	public static void addOWinner() {
		System.out.println("\n===== O is the winner!! =====\n");
		round=10;	
	}
	public static void checkDraw(char[][] board) {
		checkOdraw(board);
		checkXdraw(board);
	}
	public static void checkOdraw(char[][] board) {
		int r =0;
		int c =0;
		if(round==9 && !((board[r][c] == 'O' && board[r][c+1] == 'O' && board[r][c+2] == 'O') ||
		  (board[r][c] == 'O' && board[r+1][c] == 'O' && board[r+2][c] == 'O') || 
		  (board[r][c] == 'O' && board[r+1][c+1] == 'O' && board[r+2][c + 2] == 'O') || 
		  (board[r+1][c] == 'O' && board[r+1][c+1] == 'O' && board[r+1][c+2] == 'O') || 
		  (board[r+2][c] == 'O' && board[r+2][c+1] == 'O' && board[r+2][c+2] == 'O') || 
		  (board[r][c+1] == 'O' && board[r+1][c+1] == 'O' && board[r+2][c+1] == 'O') || 
		  (board[r][c+2] == 'O' && board[r+1][c+2] == 'O' && board[r+2][c+2] == 'O') || 
		  (board[r][c+2] == 'O' && board[r+1][c+1] == 'O'&& board[r+2][c] == 'O'))) {
			showDraw();
		}
	}
	public static void checkXdraw(char[][] board) {
		int r =0;
		int c =0;
		if(round==9 && !((board[r][c] == 'X' && board[r][c+1] == 'X' && board[r][c+2] == 'X')|| 
			(board[r][c] == 'X' && board[r+1][c] == 'X' && board[r+2][c] == 'X') || 
			(board[r][c] == 'X' && board[r+1][c+1] == 'X' && board[r+2][c + 2] == 'X') || 
			(board[r+1][c] == 'X' && board[r+1][c+1] == 'X' && board[r+1][c+2] == 'X') || 
			(board[r+2][c] == 'X' && board[r+2][c+1] == 'X' && board[r+2][c+2] == 'X') || 
			(board[r][c+1] == 'X' && board[r+1][c+1] == 'X' && board[r+2][c+1] == 'X') || 
			(board[r][c+2] == 'X' && board[r+1][c+2] == 'X' && board[r+2][c+2] == 'X') || 
			(board[r][c+2] == 'X' && board[r+1][c+1] == 'X'&& board[r+2][c] == 'X'))) {
			showDraw();
		}
	}
	public static void showDraw() {
		System.out.println("\n===== Draw!! =====\n");
	}
	public static void newBoard(char[][] board) {
		board = new char[3][3];
		for(int i=0;i<3;i++) {
			for(int j =0;j<3;j++) {
				board[i][j] = '-';
			}
		}
	}

}



